insert into CMS_USER(password,role_type,username) values('04501A7FA451F7EE0B045553553D645DB49C4A19B459F10636E8BB1A35DEDEA4','normalUser','Yanshi');
insert into CMS_USER(password,role_type,username) values('04501A7FA451F7EE0B045553553D645DB49C4A19B459F10636E8BB1A35DEDEA4','admin','fit5042');

insert into ADMINISTRATOR(ADMIN_ADDRESS,password,ADMIN_PHONE,role_type,username) 
values('3/244 koornang Rd VIC 3163','04501A7FA451F7EE0B045553553D645DB49C4A19B459F10636E8BB1A35DEDEA4',0420507833,'admin','fit5042');

insert into NORMAL_USER(DOB,password,role_type,salary,username,admin_id) 
values('1995-10-03','04501A7FA451F7EE0B045553553D645DB49C4A19B459F10636E8BB1A35DEDEA4','normalUser',8000.00,'Yanshi',1);
insert into NORMAL_USER(DOB,password,role_type,salary,username,admin_id) 
values('1997-11-03','04501A7FA451F7EE0B045553553D645DB49C4A19B459F10636E8BB1A35DEDEA4','normalUser',9000.00,'winnie',1);
insert into NORMAL_USER(DOB,password,role_type,salary,username,admin_id) 
values('2008-09-11','04501A7FA451F7EE0B045553553D645DB49C4A19B459F10636E8BB1A35DEDEA4','normalUser',5000.00,'Mia',1);
insert into NORMAL_USER(DOB,password,role_type,salary,username,admin_id) 
values('1999-11-01','04501A7FA451F7EE0B045553553D645DB49C4A19B459F10636E8BB1A35DEDEA4','normalUser',7000.00,'Tonny',1);
insert into NORMAL_USER(DOB,password,role_type,salary,username,admin_id) 
values('1990-01-20','04501A7FA451F7EE0B045553553D645DB49C4A19B459F10636E8BB1A35DEDEA4','normalUser',8435.99,'Sujit',1);
insert into NORMAL_USER(DOB,password,role_type,salary,username,admin_id) 
values('1989-06-11','04501A7FA451F7EE0B045553553D645DB49C4A19B459F10636E8BB1A35DEDEA4','normalUser',5000.00,'Kevin',1);
insert into INDUSTRY_TYPE (industryType) values ('Bank');
insert into INDUSTRY_TYPE (industryType) values ('Building');
insert into INDUSTRY_TYPE (industryType) values ('Data Communication');
insert into INDUSTRY_TYPE (industryType) values ('Education');
insert into INDUSTRY_TYPE (industryType) values ('Farm');
insert into INDUSTRY_TYPE (industryType) values ('Health');
insert into INDUSTRY_TYPE (industryType) values ('Mining');
insert into INDUSTRY_TYPE (industryType) values ('Publishing');

insert into CUSTOMER (ceo, type_id, no_of_people, purchased_product, register_date, website, postcode, state, street_address, street_number, suburb, staff_id) values ('Jack MA',1,8067,'Epson Printers','1998-11-11','www.alibaba.com','3003','Victoria','72 Appleton Dock Rd','38','West Melbourne',1);
insert into CUSTOMER (ceo, type_id, no_of_people, purchased_product, register_date, website, postcode, state, street_address, street_number, suburb, staff_id) values ('Philomena Henri', 7, 13652, 'Continuous Ink Tank Printers', '2019-07-16', 'https://google.cn', '8045', 'Victoria', '977 Maywood Parkway', '40', 'Southbank', 1);
insert into CUSTOMER (ceo, type_id, no_of_people, purchased_product, register_date, website, postcode, state, street_address, street_number, suburb, staff_id) values ('Garret Stickles', 7, 63755, 'Multifunction Printers', '2011-07-05', 'https://nyu.edu', '8045', 'Victoria', '4 Sherman Plaza', '4686', 'Parkville', 1);
insert into CUSTOMER (ceo, type_id, no_of_people, purchased_product, register_date, website, postcode, state, street_address, street_number, suburb, staff_id) values ('Fabe Somerfield', 5, 64321, 'Wide Format Printers', '2010-12-21', 'https://zdnet.com', '8045', 'Victoria', '24 Derek Trail', '82', 'South Yarra - west', 1);
insert into CUSTOMER (ceo, type_id, no_of_people, purchased_product, register_date, website, postcode, state, street_address, street_number, suburb, staff_id) values ('Katy Sandell', 6, 28314, 'Epson Printers', '2007-10-04', 'https://vk.com', '8045', 'Victoria', '43235 Lake View Avenue', '091', 'South Yarra - west', 1);
insert into CUSTOMER (ceo, type_id, no_of_people, purchased_product, register_date, website, postcode, state, street_address, street_number, suburb, staff_id) values ('Siegfried Jills', 5, 84165, 'Canon Printers', '2016-08-23', 'http://nature.com', '8045', 'Victoria', '83 Ruskin Drive', '93', 'Carlton', 1);
insert into CUSTOMER (ceo, type_id, no_of_people, purchased_product, register_date, website, postcode, state, street_address, street_number, suburb, staff_id) values ('Sonny Jimpson', 2, 43414, 'Printer Accessories', '2001-11-30', 'https://4shared.com', '8045', 'Victoria', '5640 Barnett Lane', '6', 'West Melbourne', 1);
insert into CUSTOMER (ceo, type_id, no_of_people, purchased_product, register_date, website, postcode, state, street_address, street_number, suburb, staff_id) values ('Erinna Obin', 2, 93990, 'Multifunction Printers', '2019-12-15', 'http://patch.com', '8383', 'Victoria', '68 Glacier Hill Street', '87932', 'West Melbourne', 1);
insert into CUSTOMER (ceo, type_id, no_of_people, purchased_product, register_date, website, postcode, state, street_address, street_number, suburb, staff_id) values ('Loralee O''Towey', 7, 77053, 'Brother Printers', '2012-06-22', 'http://nsw.gov.au', '8045', 'Victoria', '84347 Clyde Gallagher Parkway', '1', 'Docklands', 1);
insert into CUSTOMER (ceo, type_id, no_of_people, purchased_product, register_date, website, postcode, state, street_address, street_number, suburb, staff_id) values ('Carson Gammidge', 1, 75876, 'HP Printers', '2012-04-10', 'https://fema.gov', '8383', 'Victoria', '94395 Village Green Place', '618', 'Southbank', 1);
insert into CUSTOMER (ceo, type_id, no_of_people, purchased_product, register_date, website, postcode, state, street_address, street_number, suburb, staff_id) values ('Winslow Greenman', 1, 74271, 'Wide Format Printers', '2007-04-19', 'https://cisco.com', '8383', 'Victoria', '160 Killdeer Hill', '3802', 'Southbank', 1);
insert into CUSTOMER (ceo, type_id, no_of_people, purchased_product, register_date, website, postcode, state, street_address, street_number, suburb, staff_id) values ('Leora McCarly', 4, 14815, 'Brother Printers', '2000-09-22', 'http://nature.com', '8383', 'Victoria', '7897 Riverside Drive', '72101', 'Melbourne', 1);
insert into CUSTOMER (ceo, type_id, no_of_people, purchased_product, register_date, website, postcode, state, street_address, street_number, suburb, staff_id) values ('Leonie Tubby', 8, 85323, 'Business Printers & Copiers', '2016-02-06', 'http://telegraph.co.uk', '8045', 'Victoria', '80 Moland Lane', '2071', 'North Melbourne', 1);
insert into CUSTOMER (ceo, type_id, no_of_people, purchased_product, register_date, website, postcode, state, street_address, street_number, suburb, staff_id) values ('Dan De Angelis', 7, 85472, 'Brother Printers', '2000-08-24', 'http://cbslocal.com', '8383', 'Victoria', '611 Buell Drive', '15', 'Southbank', 1);
insert into CUSTOMER (ceo, type_id, no_of_people, purchased_product, register_date, website, postcode, state, street_address, street_number, suburb, staff_id) values ('Steffie des Remedios', 1, 52430, 'Inkjet Printers', '2018-11-04', 'http://tiny.cc', '8383', 'Victoria', '9190 Hayes Terrace', '6', 'East Melbourne', 1);
insert into CUSTOMER (ceo, type_id, no_of_people, purchased_product, register_date, website, postcode, state, street_address, street_number, suburb, staff_id) values ('Fernandina Mathe', 4, 49913, 'Fax Machines', '2006-04-24', 'https://walmart.com', '8383', 'Victoria', '2 Paget Trail', '58', 'Southbank', 1);
insert into CUSTOMER (ceo, type_id, no_of_people, purchased_product, register_date, website, postcode, state, street_address, street_number, suburb, staff_id) values ('Birch Lutty', 7, 69214, 'Fax Machines', '2015-03-08', 'http://sogou.com', '8045', 'Victoria', '63699 Moose Court', '27528', 'North Melbourne', 1);
insert into CUSTOMER (ceo, type_id, no_of_people, purchased_product, register_date, website, postcode, state, street_address, street_number, suburb, staff_id) values ('Mignon Cherry Holme', 5, 79772, 'Continuous Ink Tank Printers', '2020-01-18', 'http://angelfire.com', '8383', 'Victoria', '38047 Raven Avenue', '55209', 'Parkville', 1);
insert into CUSTOMER (ceo, type_id, no_of_people, purchased_product, register_date, website, postcode, state, street_address, street_number, suburb, staff_id) values ('Travers Denning', 4, 21923, 'Canon Printers', '2006-01-27', 'http://cpanel.net', '8045', 'Victoria', '05 Bluejay Road', '7881', 'North Melbourne', 1);
insert into CUSTOMER (ceo, type_id, no_of_people, purchased_product, register_date, website, postcode, state, street_address, street_number, suburb, staff_id) values ('Alasteir Sprigging', 4, 90672, 'Multifunction Printers', '2010-12-09', 'https://indiegogo.com', '8383', 'Victoria', '338 Graedel Way', '1753', 'Kensington and Flemington', 1);
insert into CUSTOMER (ceo, type_id, no_of_people, purchased_product, register_date, website, postcode, state, street_address, street_number, suburb, staff_id) values ('Kristien Philippson', 1, 70102, 'Inkjet Printers', '2019-03-18', 'https://domainmarket.com', '8045', 'Victoria', '92913 Kenwood Way', '4', 'Carlton', 1);
insert into CUSTOMER (ceo, type_id, no_of_people, purchased_product, register_date, website, postcode, state, street_address, street_number, suburb, staff_id) values ('Cornelia Ziemen', 7, 83527, 'Laser Printers', '2006-08-28', 'https://microsoft.com', '8045', 'Victoria', '49 Pennsylvania Drive', '81', 'Docklands', 1);
insert into CUSTOMER (ceo, type_id, no_of_people, purchased_product, register_date, website, postcode, state, street_address, street_number, suburb, staff_id) values ('Marsha Dosedale', 1, 71899, 'Brother Printers', '2014-03-14', 'http://macromedia.com', '8045', 'Victoria', '657 Homewood Trail', '4', 'Docklands', 1);
insert into CUSTOMER (ceo, type_id, no_of_people, purchased_product, register_date, website, postcode, state, street_address, street_number, suburb, staff_id) values ('Gilberta Malpass', 4, 51905, 'Wide Format Printers', '2007-03-15', 'https://census.gov', '8383', 'Victoria', '358 Bluejay Place', '1', 'Carlton', 1);
insert into CUSTOMER (ceo, type_id, no_of_people, purchased_product, register_date, website, postcode, state, street_address, street_number, suburb, staff_id) values ('Luella Brealey', 4, 51447, 'Epson Printers', '2014-11-11', 'http://google.com', '8383', 'Victoria', '07565 Fisk Terrace', '13484', 'East Melbourne', 1);
insert into CUSTOMER (ceo, type_id, no_of_people, purchased_product, register_date, website, postcode, state, street_address, street_number, suburb, staff_id) values ('Margalo McCathie', 2, 37208, 'Fax Machines', '2009-04-27', 'http://people.com.cn', '8045', 'Victoria', '9 Derek Point', '46', 'Parkville', 1);
insert into CUSTOMER (ceo, type_id, no_of_people, purchased_product, register_date, website, postcode, state, street_address, street_number, suburb, staff_id) values ('Mab Cattrell', 5, 60500, 'Printer Accessories', '2011-04-05', 'http://auda.org.au', '8383', 'Victoria', '6378 South Trail', '7904', 'Carlton', 1);
insert into CUSTOMER (ceo, type_id, no_of_people, purchased_product, register_date, website, postcode, state, street_address, street_number, suburb, staff_id) values ('Frankie Bricket', 7, 67129, 'Epson Printers', '2006-08-02', 'http://amazon.de', '8383', 'Victoria', '3 Hazelcrest Road', '1878', 'North Melbourne', 1);
insert into CUSTOMER (ceo, type_id, no_of_people, purchased_product, register_date, website, postcode, state, street_address, street_number, suburb, staff_id) values ('Vinita Lightman', 7, 86665, 'HP Printers', '2014-07-03', 'http://theguardian.com', '8383', 'Victoria', '797 Ohio Street', '216', 'Docklands', 1);
insert into CUSTOMER (ceo, type_id, no_of_people, purchased_product, register_date, website, postcode, state, street_address, street_number, suburb, staff_id) values ('Bobette Werndly', 5, 30844, 'Brother Printers', '2012-09-29', 'http://wired.com', '8045', 'Victoria', '02 Spaight Court', '84', 'Parkville', 1);
insert into CUSTOMER (ceo, type_id, no_of_people, purchased_product, register_date, website, postcode, state, street_address, street_number, suburb, staff_id) values ('Melisent Bazely', 7, 19375, 'Epson Printers', '2019-09-18', 'https://apache.org', '8383', 'Victoria', '9805 Bluejay Court', '7796', 'Port Melbourne', 1);
insert into CUSTOMER (ceo, type_id, no_of_people, purchased_product, register_date, website, postcode, state, street_address, street_number, suburb, staff_id) values ('Salim Poetz', 1, 73193, 'Continuous Ink Tank Printers', '1999-03-15', 'https://census.gov', '8045', 'Victoria', '0 Clyde Gallagher Alley', '41', 'East Melbourne', 1);
insert into CUSTOMER (ceo, type_id, no_of_people, purchased_product, register_date, website, postcode, state, street_address, street_number, suburb, staff_id) values ('Kalvin Bowlands', 8, 78526, 'A3 Printers', '2014-12-10', 'https://apple.com', '8383', 'Victoria', '4890 Badeau Alley', '30906', 'South Yarra - west', 1);
insert into CUSTOMER (ceo, type_id, no_of_people, purchased_product, register_date, website, postcode, state, street_address, street_number, suburb, staff_id) values ('Constantia Manktelow', 1, 23389, 'Fax Machines', '2016-04-08', 'http://gov.uk', '8383', 'Victoria', '1980 Hooker Parkway', '3', 'West Melbourne', 1);
insert into CUSTOMER (ceo, type_id, no_of_people, purchased_product, register_date, website, postcode, state, street_address, street_number, suburb, staff_id) values ('Georgeta Garbar', 6, 91617, 'Wide Format Printers', '2004-02-18', 'http://51.la', '8045', 'Victoria', '7 Tennessee Place', '1486', 'Port Melbourne', 1);
insert into CUSTOMER (ceo, type_id, no_of_people, purchased_product, register_date, website, postcode, state, street_address, street_number, suburb, staff_id) values ('Inigo Emanson', 3, 19404, 'Wide Format Printers', '2016-04-26', 'http://sogou.com', '8045', 'Victoria', '21820 Di Loreto Park', '253', 'Melbourne', 1);
insert into CUSTOMER (ceo, type_id, no_of_people, purchased_product, register_date, website, postcode, state, street_address, street_number, suburb, staff_id) values ('Dita Evangelinos', 8, 44781, 'Wide Format Printers', '2008-07-05', 'https://census.gov', '8045', 'Victoria', '0 Express Terrace', '6', 'Southbank', 1);
insert into CUSTOMER (ceo, type_id, no_of_people, purchased_product, register_date, website, postcode, state, street_address, street_number, suburb, staff_id) values ('Morgan Tichelaar', 7, 49745, 'Epson Printers', '2014-08-11', 'http://merriam-webster.com', '8383', 'Victoria', '780 Tennyson Lane', '5', 'East Melbourne', 1);
insert into CUSTOMER (ceo, type_id, no_of_people, purchased_product, register_date, website, postcode, state, street_address, street_number, suburb, staff_id) values ('Karoline Chaulk', 3, 23283, 'Printer Accessories', '2011-06-17', 'https://ihg.com', '8383', 'Victoria', '00998 New Castle Drive', '476', 'Southbank', 1);
insert into CUSTOMER (ceo, type_id, no_of_people, purchased_product, register_date, website, postcode, state, street_address, street_number, suburb, staff_id) values ('Salomone Wallenger', 6, 68441, 'Fax Machines', '2011-03-04', 'https://live.com', '8383', 'Victoria', '37 Fordem Terrace', '941', 'Carlton', 1);
insert into CUSTOMER (ceo, type_id, no_of_people, purchased_product, register_date, website, postcode, state, street_address, street_number, suburb, staff_id) values ('Shaine Bollum', 2, 42666, 'Laser Printers', '2017-02-19', 'http://ox.ac.uk', '8045', 'Victoria', '7 Service Place', '94', 'Port Melbourne', 1);
insert into CUSTOMER (ceo, type_id, no_of_people, purchased_product, register_date, website, postcode, state, street_address, street_number, suburb, staff_id) values ('Ravid Leborgne', 4, 96940, 'Fax Machines', '2013-11-24', 'http://epa.gov', '8045', 'Victoria', '0104 Brentwood Street', '71', 'Kensington and Flemington', 1);
insert into CUSTOMER (ceo, type_id, no_of_people, purchased_product, register_date, website, postcode, state, street_address, street_number, suburb, staff_id) values ('Reginald Lathan', 7, 90556, 'Inkjet Printers', '2002-07-19', 'https://nyu.edu', '8383', 'Victoria', '17 Westerfield Alley', '8134', 'North Melbourne', 1);
insert into CUSTOMER (ceo, type_id, no_of_people, purchased_product, register_date, website, postcode, state, street_address, street_number, suburb, staff_id) values ('Nikolai Tourville', 5, 12966, 'Laser Printers', '1999-04-09', 'https://shinystat.com', '8045', 'Victoria', '3895 Esch Plaza', '60', 'North Melbourne', 1);
insert into CUSTOMER (ceo, type_id, no_of_people, purchased_product, register_date, website, postcode, state, street_address, street_number, suburb, staff_id) values ('Emeline Gornal', 7, 35939, 'Printer Accessories', '2007-01-23', 'https://biblegateway.com', '8045', 'Victoria', '9683 Homewood Park', '08', 'East Melbourne', 1);
insert into CUSTOMER (ceo, type_id, no_of_people, purchased_product, register_date, website, postcode, state, street_address, street_number, suburb, staff_id) values ('Alwyn Krol', 2, 92848, 'Business Printers & Copiers', '2008-03-08', 'https://zdnet.com', '8045', 'Victoria', '8018 Del Mar Park', '7657', 'North Melbourne', 1);
insert into CUSTOMER (ceo, type_id, no_of_people, purchased_product, register_date, website, postcode, state, street_address, street_number, suburb, staff_id) values ('Lanny Rose', 4, 89105, 'Multifunction Printers', '2019-09-28', 'http://illinois.edu', '8383', 'Victoria', '3948 Amoth Junction', '0903', 'Southbank', 1);
insert into CUSTOMER (ceo, type_id, no_of_people, purchased_product, register_date, website, postcode, state, street_address, street_number, suburb, staff_id) values ('Ricki Rowlings', 4, 60256, 'HP Printers', '2003-06-25', 'http://123-reg.co.uk', '8383', 'Victoria', '5745 Dryden Street', '4947', 'Kensington and Flemington', 1);
insert into CUSTOMER (ceo, type_id, no_of_people, purchased_product, register_date, website, postcode, state, street_address, street_number, suburb, staff_id) values ('Luis Hammel', 7, 11670, 'Printer Accessories', '2007-11-16', 'http://dagondesign.com', '8045', 'Victoria', '92 Bartillon Point', '743', 'East Melbourne', 1);
insert into CUSTOMER (ceo, type_id, no_of_people, purchased_product, register_date, website, postcode, state, street_address, street_number, suburb, staff_id) values ('Leonard Coundley', 2, 34967, 'Inkjet Printers', '2011-10-01', 'https://cyberchimps.com', '8045', 'Victoria', '7 Mayer Drive', '86', 'West Melbourne', 1);
insert into CUSTOMER (ceo, type_id, no_of_people, purchased_product, register_date, website, postcode, state, street_address, street_number, suburb, staff_id) values ('Everard Bearn', 7, 81679, 'Continuous Ink Tank Printers', '2019-03-12', 'https://gizmodo.com', '8045', 'Victoria', '94238 2nd Court', '1505', 'Southbank', 1);


insert into CUSTOMER_CONTACT (AGE, AVAILABLE_TIME, GENDER, MOBILE_PHONE, NAME, CUS_ID) values(40, 'Monday-Friday 9:00-14:00', 'Male', 123214, 'liuyanshi', 3);
insert into CUSTOMER_CONTACT (AGE, AVAILABLE_TIME, GENDER, MOBILE_PHONE, NAME, CUS_ID) values(41, 'Monday Tuesday 9:00-14:00', 'Female', 123214, 'Vivian', 3);	
insert into CUSTOMER_CONTACT (AGE, AVAILABLE_TIME, GENDER, MOBILE_PHONE, NAME, CUS_ID) values(35, 'Monday-Friday  14:00-16:00', 'Male', 33453465, 'xiaoming', 4);


	
CREATE TABLE GROUPS
(
id bigint NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
username varchar(150) not null,
groupname varchar(150) not null,
primary key (id)
);
insert into GROUPS (username, groupname) values(
'fit5042',
'admin'
);
