package controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import controllers.CustomerApplication;
import controllers.Customer;
import mbeans.CustomerManagedBean;

@RequestScoped
@Named("removeCustomer")
public class DeleteCustomer {
	@ManagedProperty(value = "#{propertyManagedBean}")
    CustomerManagedBean customerManagedBean;

    private boolean showForm = true;

    private Customer customer;

    CustomerApplication app;

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Customer getCustomer() {
        return customer;
    }

    public boolean isShowForm() {
        return showForm;
    }

    public DeleteCustomer() {
        ELContext context
                = FacesContext.getCurrentInstance().getELContext();

        app = (CustomerApplication) FacesContext.getCurrentInstance()
                .getApplication()
                .getELResolver()
                .getValue(context, null, "customerApplication");

        app.updateCustomerList();

        //instantiate customerManagedBean
        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        customerManagedBean = (CustomerManagedBean) FacesContext.getCurrentInstance().getApplication()
                .getELResolver().getValue(elContext, null, "customerManagedBean");
    }

    /**
     * @param customer Id
     */
    public void removeCustomer(int cusId) {
        try {
            //remove this customer from db via EJB
        	customerManagedBean.removeCustomer(cusId);

            //refresh the list in CustomerApplication bean
             app.searchAll();
//             FacesMessage message = new FacesMessage("Customer has been deleted succesfully");
//             FacesContext context = FacesContext.getCurrentInstance();
//             context.addMessage(delete.getClientId(context), message);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Customer has been deleted succesfully"));
        } catch (Exception ex) {

        }
        showForm = true;
    }
}
