package controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import controllers.Customer;
import controllers.CustomerApplication;

@RequestScoped
@Named("searchCustomer")
public class SearchCustomer {
	
	private boolean showForm = true;

    private Customer customer;

    CustomerApplication app;

    private String searchByType;
    private String searchCustomerByProduct;
	/**
	 * @return the searchCustomerByProduct
	 */
	public String getSearchCustomerByProduct() {
		return searchCustomerByProduct;
	}

	/**
	 * @param searchCustomerByProduct the searchCustomerByProduct to set
	 */
	public void setSearchCustomerByProduct(String searchCustomerByProduct) {
		this.searchCustomerByProduct = searchCustomerByProduct;
	}


    public CustomerApplication getApp() {
        return app;
    }

    public void setApp(CustomerApplication app) {
        this.app = app;
    }

	/**
	 * @return the searchByType
	 */
	public String getSearchByType() {
		return searchByType;
	}

	/**
	 * @param searchByType the searchByType to set
	 */
	public void setSearchByType(String searchByType) {
		this.searchByType = searchByType;
	}
	
    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Customer getCustomer() {
        return customer;
    }

    public boolean isShowForm() {
        return showForm;
    }

    public SearchCustomer() {
        ELContext context
                = FacesContext.getCurrentInstance().getELContext();

        app = (CustomerApplication) FacesContext.getCurrentInstance()
                .getApplication()
                .getELResolver()
                .getValue(context, null, "customerApplication");

        app.updateCustomerList();

    }

    /**
     * @param industry type
     */
    public void searchCustomerByType(String type) {
        try {
            if(type.equals("all")) {
            	app.searchAll();
            }
            else
            {
            	int a = Integer.parseInt(type);
            	app.searchCustomerByType(a);//search by type then refresh the list in CustomerApplication bean
            }
        } catch (Exception ex) {

        }
        showForm = true;

    }
    
    /**
     * @param Purchased Product
     */
    public void searchByPurchasedProduct(String product) {
        try {
            if(product.equals("all")) {
            	app.searchAll();
            }
            else
            	app.searchCustomerByProduct(product);//search by type then refresh the list in CustomerApplication bean
        } catch (Exception ex) {

        }
        showForm = true;

    }

    public void searchAll() {
        try {
            //return all properties from db via EJB
            app.searchAll();
        } catch (Exception ex) {

        }
        showForm = true;
    }
}
