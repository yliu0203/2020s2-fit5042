package controllers;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import cms.entities.Address;
import cms.entities.CustomerContact;
import cms.entities.IndustryType;
import cms.entities.NormalUser;

@RequestScoped
@Named(value = "customer")
public class Customer implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int cusId;
	private String ceo;
	private IndustryType industryType;
	private int noOfPeople;
	private String purchasedProduct;
	private Date registerDate;
	private String website;
	private Set<CustomerContact> customerContacts;
	private Address address;
	private NormalUser normalUser;
    private String streetNumber;
    private String streetAddress;
    private String suburb;
    private String postcode;
    private String state;
    public Customer() {
    	
    }
	public Customer(int cusId, String ceo, IndustryType industryType, int noOfPeople, String purchasedProduct,
			Date registerDate, String website, Set<CustomerContact> customerContacts, Address address,
			NormalUser normalUser, String streetNumber, String streetAddress, String suburb, String postcode,
			String state) {
		this.cusId = cusId;
		this.ceo = ceo;
		this.industryType = industryType;
		this.noOfPeople = noOfPeople;
		this.purchasedProduct = purchasedProduct;
		this.registerDate = registerDate;
		this.website = website;
		this.customerContacts = customerContacts;
		this.address = address;
		this.normalUser = normalUser;
		this.streetNumber = streetNumber;
		this.streetAddress = streetAddress;
		this.suburb = suburb;
		this.postcode = postcode;
		this.state = state;
	}
	/**
	 * @return the cusId
	 */
	public int getCusId() {
		return cusId;
	}
	/**
	 * @param cusId the cusId to set
	 */
	public void setCusId(int cusId) {
		this.cusId = cusId;
	}
	/**
	 * @return the ceo
	 */
	public String getCeo() {
		return ceo;
	}
	/**
	 * @param ceo the ceo to set
	 */
	public void setCeo(String ceo) {
		this.ceo = ceo;
	}
	/**
	 * @return the industryType
	 */
	public IndustryType getIndustryType() {
		return industryType;
	}
	/**
	 * @param industryType the industryType to set
	 */
	public void setIndustryType(IndustryType industryType) {
		this.industryType = industryType;
	}
	/**
	 * @return the noOfPeople
	 */
	public int getNoOfPeople() {
		return noOfPeople;
	}
	/**
	 * @param noOfPeople the noOfPeople to set
	 */
	public void setNoOfPeople(int noOfPeople) {
		this.noOfPeople = noOfPeople;
	}
	/**
	 * @return the purchasedProduct
	 */
	public String getPurchasedProduct() {
		return purchasedProduct;
	}
	/**
	 * @param purchasedProduct the purchasedProduct to set
	 */
	public void setPurchasedProduct(String purchasedProduct) {
		this.purchasedProduct = purchasedProduct;
	}
	/**
	 * @return the registerDate
	 */
	public Date getRegisterDate() {
		return registerDate;
	}
	/**
	 * @param registerDate the registerDate to set
	 */
	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}
	/**
	 * @return the website
	 */
	public String getWebsite() {
		return website;
	}
	/**
	 * @param website the website to set
	 */
	public void setWebsite(String website) {
		this.website = website;
	}
	/**
	 * @return the customerContacts
	 */
	public Set<CustomerContact> getCustomerContacts() {
		return customerContacts;
	}
	/**
	 * @param customerContacts the customerContacts to set
	 */
	public void setCustomerContacts(Set<CustomerContact> customerContacts) {
		this.customerContacts = customerContacts;
	}
	/**
	 * @return the address
	 */
	public Address getAddress() {
		return address;
	}
	/**
	 * @param address the address to set
	 */
	public void setAddress(Address address) {
		this.address = address;
	}
	/**
	 * @return the normalUser
	 */
	public NormalUser getNormalUser() {
		return normalUser;
	}
	/**
	 * @param normalUser the normalUser to set
	 */
	public void setNormalUser(NormalUser normalUser) {
		this.normalUser = normalUser;
	}
	/**
	 * @return the streetNumber
	 */
	public String getStreetNumber() {
		return streetNumber;
	}
	/**
	 * @param streetNumber the streetNumber to set
	 */
	public void setStreetNumber(String streetNumber) {
		this.streetNumber = streetNumber;
	}
	/**
	 * @return the streetAddress
	 */
	public String getStreetAddress() {
		return streetAddress;
	}
	/**
	 * @param streetAddress the streetAddress to set
	 */
	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}
	/**
	 * @return the suburb
	 */
	public String getSuburb() {
		return suburb;
	}
	/**
	 * @param suburb the suburb to set
	 */
	public void setSuburb(String suburb) {
		this.suburb = suburb;
	}
	/**
	 * @return the postcode
	 */
	public String getPostcode() {
		return postcode;
	}
	/**
	 * @param postcode the postcode to set
	 */
	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}
	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}
	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}
	@Override
	public String toString() {
		return "Customer [cusId=" + cusId + ", ceo=" + ceo + ", industryType=" + industryType.getIndustryType() + ", noOfPeople="
				+ noOfPeople + ", purchasedProduct=" + purchasedProduct + ", registerDate=" + registerDate
				+ ", website=" + website + ", customerContacts=" + customerContacts + ", address=" + address
				+ ", normalUser=" + normalUser + ", streetNumber=" + streetNumber + ", streetAddress=" + streetAddress
				+ ", suburb=" + suburb + ", postcode=" + postcode + ", state=" + state + "]";
	}

}
