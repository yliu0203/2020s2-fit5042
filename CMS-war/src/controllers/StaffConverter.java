package controllers;

import java.util.List;

import javax.el.ELContext;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

import mbeans.CustomerManagedBean;
import cms.entities.NormalUser;

@FacesConverter(forClass = cms.entities.NormalUser.class, value = "Staff")
public class StaffConverter implements Converter{
	@ManagedProperty(value = "#{propertyManagedBean}")
    CustomerManagedBean custoemrManagedBean;

    public List<NormalUser> NormalUserList;

    public StaffConverter() {
        try {
            //instantiate propertyManagedBean
            ELContext elContext = FacesContext.getCurrentInstance().getELContext();
            custoemrManagedBean = (CustomerManagedBean) FacesContext.getCurrentInstance().getApplication()
                    .getELResolver().getValue(elContext, null, "customerManagedBean");

            NormalUserList = custoemrManagedBean.getAllStaff();
        } catch (Exception ex) {

        }
    }

    //this method is for converting the submitted value (as String) to the NormalUser object
    //the reason for using this method is, the dropdown box in the xhtml does not capture the NormalUser object, but the String.
    public NormalUser getAsObject(FacesContext facesContext, UIComponent component, String submittedValue) {
        if (submittedValue.trim().equals("")) {
            return null;
        } else {
            try {
                int number = Integer.parseInt(submittedValue);

                for (NormalUser c : NormalUserList) {
                    if (c.getId() == number) {
                        return c;
                    }
                }

            } catch (NumberFormatException exception) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid staff"));
            }
        }

        return null;
    }

    public String getAsString(FacesContext facesContext, UIComponent component, Object value) {
        if (value == null || value.equals("")) {
            return "";
        } else {
            return String.valueOf(((NormalUser) value).getId());
        }
    }

}
