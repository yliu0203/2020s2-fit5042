package controllers;

import java.io.Serializable;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import cms.entities.Customer;

@RequestScoped
@Named(value = "contactPerson")
public class ContactPerson implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int contactId;
	private int age;
	private String availableTime;
	private String gender;
	private int mobilePhone;
	private String name;
	private Customer customer;
	public ContactPerson() {
		
	}
	
	public ContactPerson(int contactId, int age, String availableTime, String gender, int mobilePhone, String name,
			Customer customer) {
		this.contactId = contactId;
		this.age = age;
		this.availableTime = availableTime;
		this.gender = gender;
		this.mobilePhone = mobilePhone;
		this.name = name;
		this.customer = customer;
	}
	/**
	 * @return the contactId
	 */
	public int getContactId() {
		return contactId;
	}
	/**
	 * @param contactId the contactId to set
	 */
	public void setContactId(int contactId) {
		this.contactId = contactId;
	}
	/**
	 * @return the age
	 */
	public int getAge() {
		return age;
	}
	/**
	 * @param age the age to set
	 */
	public void setAge(int age) {
		this.age = age;
	}
	/**
	 * @return the availableTime
	 */
	public String getAvailableTime() {
		return availableTime;
	}
	/**
	 * @param availableTime the availableTime to set
	 */
	public void setAvailableTime(String availableTime) {
		this.availableTime = availableTime;
	}
	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}
	/**
	 * @param gender the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}
	/**
	 * @return the mobilePhone
	 */
	public int getMobilePhone() {
		return mobilePhone;
	}
	/**
	 * @param mobilePhone the mobilePhone to set
	 */
	public void setMobilePhone(int mobilePhone) {
		this.mobilePhone = mobilePhone;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the customer
	 */
	public Customer getCustomer() {
		return customer;
	}
	/**
	 * @param customer the customer to set
	 */
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	@Override
	public String toString() {
		return "ContactPerson [contactId=" + contactId + ", age=" + age + ", availableTime=" + availableTime
				+ ", gender=" + gender + ", mobilePhone=" + mobilePhone + ", name=" + name + ", customer=" + customer
				+ "]";
	}

}
