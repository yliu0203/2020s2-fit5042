package controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import mbeans.CustomerManagedBean;

@Named(value = "industryController")
@RequestScoped
public class IndustryController {
	@ManagedProperty(value = "#{propertyManagedBean}")
	CustomerManagedBean customerManagedBean;
	
	private cms.entities.IndustryType industryType;
	private int typeId;
	
	public IndustryController() {
		// Assign customer identifier via GET param
		// this customerId is the index, don't confuse with the Customer Id
		typeId = Integer.valueOf(
				FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("paramIndex"));
		// Assign customer based on the id provided
		industryType = getIndustryType();
	}

	/**
	 * @return the typeId
	 */
	public int getTypeId() {
		return typeId;
	}

	/**
	 * @param typeId the typeId to set
	 */
	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}
	
	public cms.entities.IndustryType getIndustryType() {
		if (industryType == null) {
			// Get application context bean CustomerApplication
			ELContext context = FacesContext.getCurrentInstance().getELContext();
			CustomerApplication app = (CustomerApplication) FacesContext.getCurrentInstance().getApplication()
					.getELResolver().getValue(context, null, "customerApplication");
			// -1 to propertyId since we +1 in JSF (to always have positive property id!)
			if (app.getIndustryTypes().size() >= --typeId) {
				return app.getIndustryTypes().get(typeId); // this customerId is the index, don't confuse with the
															// Property Id
			}
		}
		return industryType;
	}
}
