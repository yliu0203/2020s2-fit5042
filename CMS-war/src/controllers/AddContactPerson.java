package controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import controllers.ContactPerson;
import mbeans.CustomerManagedBean;

@RequestScoped
@Named("addContactPerson")
public class AddContactPerson {
	 @ManagedProperty(value = "#{customerManagedBean}")
	    CustomerManagedBean customerManagedBean;

	    private boolean showForm = true;
	    private UIComponent submit;

	    private ContactPerson contactPerson;

	    CustomerApplication app;

		/**
		 * @return the contactPerson
		 */
		public ContactPerson getContactPerson() {
			return contactPerson;
		}

		/**
		 * @param contactPerson the contactPerson to set
		 */
		public void setContactPerson(ContactPerson contactPerson) {
			this.contactPerson = contactPerson;
		}

		public boolean isShowForm() {
	        return showForm;
		}
	    public AddContactPerson() {
	        ELContext context
	                = FacesContext.getCurrentInstance().getELContext();

	        app = (CustomerApplication) FacesContext.getCurrentInstance()
	                .getApplication()
	                .getELResolver()
	                .getValue(context, null, "customerApplication");
	        
	        //instantiate customerManagedBean
	        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
	        customerManagedBean = (CustomerManagedBean) FacesContext.getCurrentInstance().getApplication()
	                .getELResolver().getValue(elContext, null, "customerManagedBean");
	    }

	    public void addNewContactPerson(ContactPerson localContactPerson) {
	        //this is the local customer, not the entity
	        try {
	            //add this ContactPerson to db via EJB
	        	customerManagedBean.addContactPerson(localContactPerson);
	        	
	            //refresh the list in CustomerApplication bean
	             app.searchAll1();
	             FacesMessage message = new FacesMessage("Contact Person has been added succesfully");
	             FacesContext context = FacesContext.getCurrentInstance();
	             context.addMessage(submit.getClientId(context), message);
	            //acesContext.getCurrentInstance().addMessage(null, new FacesMessage("Property has been added succesfully"));
	        } catch (Exception ex) {

	        }
	        showForm = true;
	    }

		/**
		 * @return the submit
		 */
		public UIComponent getSubmit() {
			return submit;
		}

		/**
		 * @param submit the submit to set
		 */
		public void setSubmit(UIComponent submit) {
			this.submit = submit;
		}

}
