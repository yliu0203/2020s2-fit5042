package controllers;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import cms.entities.NormalUser;
import repository.interfaces.CustomerRepository;

@Named(value = "deleteNormalUser")
@RequestScoped
public class DeleteNormalUser {
	@EJB
    CustomerRepository UserRepository;


	/**
    * Creates a new instance of UpdateUser
    */
    public DeleteNormalUser() {
    }

    public CustomerRepository getUserSB() {
        return UserRepository;
    }

    public void setUserSB(CustomerRepository UserRepository) {
        this.UserRepository = UserRepository;
    }

    public String deleteUserById(long id) throws Exception {
    	UserRepository.removeNormalUser(id);
        return "normalUser";
    }
}
