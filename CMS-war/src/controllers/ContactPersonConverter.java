package controllers;

import java.util.List;

import javax.el.ELContext;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

import cms.entities.NormalUser;
import cms.entities.Customer;
import mbeans.CustomerManagedBean;

@FacesConverter(forClass = cms.entities.NormalUser.class, value = "contactPersonConverter")
public class ContactPersonConverter implements Converter{
	@ManagedProperty(value = "#{propertyManagedBean}")
    CustomerManagedBean custoemrManagedBean;

    public List<Customer> CustomerList;

    public ContactPersonConverter() {
        try {
            //instantiate propertyManagedBean
            ELContext elContext = FacesContext.getCurrentInstance().getELContext();
            custoemrManagedBean = (CustomerManagedBean) FacesContext.getCurrentInstance().getApplication()
                    .getELResolver().getValue(elContext, null, "customerManagedBean");

            CustomerList = custoemrManagedBean.getAllCustomers();
        } catch (Exception ex) {

        }
    }

    //this method is for converting the submitted value (as String) to the NormalUser object
    //the reason for using this method is, the dropdown box in the xhtml does not capture the NormalUser object, but the String.
    public Customer getAsObject(FacesContext facesContext, UIComponent component, String submittedValue) {
        if (submittedValue.trim().equals("")) {
            return null;
        } else {
            try {
                int number = Integer.parseInt(submittedValue);

                for (Customer c : CustomerList) {
                    if (c.getCusId() == number) {
                        return c;
                    }
                }

            } catch (NumberFormatException exception) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid contact person"));
            }
        }

        return null;
    }

    public String getAsString(FacesContext facesContext, UIComponent component, Object value) {
        if (value == null || value.equals("")) {
            return "";
        } else {
            return String.valueOf(((Customer) value).getCusId());
        }
    }

}
