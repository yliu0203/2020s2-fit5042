package controllers;

import java.net.URL;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

@FacesValidator("UrlValidator")
public class UrlValidation implements Validator{
	@Override
	   public void validate(FacesContext facesContext, UIComponent component, Object value)
	      throws ValidatorException {
	      StringBuilder url = new StringBuilder();
	      String urlValue = value.toString();

	      if(!urlValue.startsWith("http://", 0)) {
	         url.append("http://");
	      }
	      url.append(urlValue);
	        /* Try creating a valid URL */
	        try { 
	            new URL(url.toString()).toURI(); 
	        }
	        // If there was an Exception 
	        // while creating URL object 
	        catch (Exception e) { 
	        	FacesMessage msg =
		 	            new FacesMessage("URL validation failed","Invalid URL format");
		 	         msg.setSeverity(FacesMessage.SEVERITY_ERROR);
		 	         throw new ValidatorException(msg);
	        }   
	   }
}
