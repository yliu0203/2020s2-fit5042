package controllers;

import java.util.ArrayList;
import java.util.List;

import javax.el.ELContext;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import mbeans.CustomerManagedBean;
import cms.entities.Customer;
import cms.entities.CustomerContact;
import cms.entities.IndustryType;

@Named(value = "customerApplication")
@ApplicationScoped
public class CustomerApplication {

    //dependency injection of managed bean here so that we can use its methods
    @ManagedProperty(value = "#{propertyManagedBean}")
    CustomerManagedBean customerManagedBean;
    
    private ArrayList<Customer> customers;
    private ArrayList<IndustryType> industryTypes;
    private ArrayList<CustomerContact> contactPerson;

    private boolean showForm = true;

    public boolean isShowForm() {
        return showForm;
    }

    // Add some property data from db to app 
    public CustomerApplication() throws Exception {
    	customers = new ArrayList<Customer>();
    	contactPerson = new ArrayList<CustomerContact>();
    	industryTypes = new ArrayList<IndustryType>();
        //instantiate propertyManagedBean
        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        customerManagedBean = (CustomerManagedBean) FacesContext.getCurrentInstance().getApplication().getELResolver().getValue(elContext, null, "customerManagedBean");

        //get properties from db 
        updateCustomerList();
        
        //get properties from db 
        updateContactPersonList();
        
        //get industry Types from db 
        updateIndustryTypesList();
    }

    /**
	 * @return the industryTypes
	 */
	public ArrayList<IndustryType> getIndustryTypes() {
		return industryTypes;
	}

	/**
	 * @param industryTypes the industryTypes to set
	 */
	public void setIndustryTypes(ArrayList<IndustryType> industryTypes) {
		this.industryTypes = industryTypes;
	}

	/**
	 * @return the contactPerson
	 */
	public ArrayList<CustomerContact> getContactPerson() {
		return contactPerson;
	}

	/**
	 * @param contactPerson the contactPerson to set
	 */
	public void setContactPerson(ArrayList<CustomerContact> contactPerson) {
		this.contactPerson = contactPerson;
	}

	public ArrayList<Customer> getCustomers() {
        return customers;
    }

    private void setCustomers(ArrayList<Customer> newCustomers) {
        this.customers = newCustomers;
    }
    //when loading, and after adding or deleting, the customer list needs to be refreshed
    //this method is for that purpose
    public void updateCustomerList() {
        if (customers != null && customers.size() > 0)
        {
            
        }
        else
        {
        	customers.clear();

            for (cms.entities.Customer customer : customerManagedBean.getAllCustomers())
            {
            	customers.add(customer);
            }
            setCustomers(customers);
        }
    }
    
    public void updateContactPersonList() {
        if (contactPerson != null && contactPerson.size() > 0)
        {
            
        }
        else
        {
        	contactPerson.clear();

            for (cms.entities.CustomerContact person : customerManagedBean.getAllContactPerson())
            {
            	contactPerson.add(person);
            }
            setContactPerson(contactPerson);
        }
    }
    
    public void updateIndustryTypesList() {
        if (industryTypes != null && industryTypes.size() > 0)
        {
            
        }
        else
        {
        	industryTypes.clear();

            for (cms.entities.IndustryType type : customerManagedBean.getAllIndustryType())
            {
            	industryTypes.add(type);
            }
            setIndustryTypes(industryTypes);
        }
    }
    
    public void searchAll()
    {
    	customers.clear();
        
        for (cms.entities.Customer customer : customerManagedBean.getAllCustomers())
        {
        	customers.add(customer);
        }
        
        setCustomers(customers);
    }
    
    public void searchAll1()
    {
    	contactPerson.clear();
        
        for (cms.entities.CustomerContact person : customerManagedBean.getAllContactPerson())
        {
        	contactPerson.add(person);
        }
        
        setContactPerson(contactPerson);
    }
    public void searchAll2()
    {
    	industryTypes.clear();
        
        for (cms.entities.IndustryType industryType : customerManagedBean.getAllIndustryType())
        {
        	industryTypes.add(industryType);
        }
        
        setIndustryTypes(industryTypes);
    }
    
    public void searchCustomerByType(int type) {
    	customers.clear();
    	List<Customer> newCustoemr = customerManagedBean.searchCustomerByType(type);
        for (cms.entities.Customer customer : newCustoemr)
        {
        	customers.add(customer);
        }
        setCustomers(customers);
    }
    
    public void searchCustomerByProduct(String product) {
    	customers.clear();
    	List<Customer> newCustoemr = customerManagedBean.searchCustomerByProduct(product);
        for (cms.entities.Customer customer : newCustoemr)
        {
        	customers.add(customer);
        }
        setCustomers(customers);
    }
}
