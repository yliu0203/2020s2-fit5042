package controllers;

import java.util.ArrayList;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import controllers.CustomerApplication;
import mbeans.CustomerManagedBean;

@Named(value = "customerController")
@RequestScoped
public class CustomerController {
	/**
	 * @return the showForm
	 */
	public boolean isShowForm() {
		return showForm;
	}

	/**
	 * @param showForm the showForm to set
	 */
	public void setShowForm(boolean showForm) {
		this.showForm = showForm;
	}

	@ManagedProperty(value = "#{propertyManagedBean}")
	CustomerManagedBean customerManagedBean;

	/**
	 * @return the contactinfor
	 */
	public cms.entities.CustomerContact getContactinfor() {
		return contactinfor;
	}

	/**
	 * @param contactinfor the contactinfor to set
	 */
	public void setContactinfor(cms.entities.CustomerContact contactinfor) {
		this.contactinfor = contactinfor;
	}

	private int customerId; // this customerId is the index, don't confuse with the Property Id
	private int contactId;

	/**
	 * @return the contactId
	 */
	public int getContactId() {
		return contactId;
	}

	/**
	 * @param contactId the contactId to set
	 */
	public void setContactId(int contactId) {
		this.contactId = contactId;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	private cms.entities.Customer customer;
	private ArrayList<cms.entities.CustomerContact> contactPerson;
	private cms.entities.CustomerContact contactinfor;
	private int index = 0;
	private int getId = 0;
	/**
	 * @return the getId
	 */
	public int getGetId() {
		return getId;
	}

	/**
	 * @param getId the getId to set
	 */
	public void setGetId(int getId) {
		this.getId = getId;
	}

	private boolean showForm = true;

	public CustomerController() {
		// Assign customer identifier via GET param
		// this customerId is the index, don't confuse with the Customer Id
		customerId = Integer.valueOf(
				FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("paramIndex"));
		// Assign customer based on the id provided
		customer = getCustomer();
		contactPerson = getContactPerson();
		contactinfor = getContactPersonInfor();
		// index = getCustomerIndex();
		getId = customerId;
	}

	public cms.entities.Customer getCustomer() {
		if (customer == null) {
			// Get application context bean CustomerApplication
			ELContext context = FacesContext.getCurrentInstance().getELContext();
			CustomerApplication app = (CustomerApplication) FacesContext.getCurrentInstance().getApplication()
					.getELResolver().getValue(context, null, "customerApplication");
			// -1 to propertyId since we +1 in JSF (to always have positive property id!)
			if (app.getCustomers().size() >= --customerId) {
				return app.getCustomers().get(customerId); // this customerId is the index, don't confuse with the
															// Property Id
			}
		}
		return customer;
	}

	public cms.entities.CustomerContact getContactPersonInfor() {
		if (contactinfor == null) {
			// Get application context bean CustomerApplication
			ELContext context = FacesContext.getCurrentInstance().getELContext();
			CustomerApplication app = (CustomerApplication) FacesContext.getCurrentInstance().getApplication()
					.getELResolver().getValue(context, null, "customerApplication");
			int index = 0;
			for (int i = 0; i < app.getContactPerson().size(); i++) {
				if (app.getContactPerson().get(i).getContactId() == customerId) {
					index = i;
					break;
				}
			}
			if(app.getContactPerson().size()==0)//make sure if no contact person infor for certain customer
				return contactinfor;
			else
				return app.getContactPerson().get(index);
		}
		return contactinfor;
	}

	public ArrayList<cms.entities.CustomerContact> getContactPerson() {
		if (contactPerson == null) {
			// Get application context bean CustomerApplication
			ELContext context = FacesContext.getCurrentInstance().getELContext();
			CustomerApplication app = (CustomerApplication) FacesContext.getCurrentInstance().getApplication()
					.getELResolver().getValue(context, null, "customerApplication");
			int id = ++customerId;
			contactPerson = new ArrayList<cms.entities.CustomerContact>();
			for (int i = 0; i < app.getContactPerson().size(); i++) {
				if (app.getContactPerson().get(i).getCustomer().getCusId() == id) {
					contactPerson.add(app.getContactPerson().get(i));
				}
			}
			return contactPerson;
		}
		return contactPerson;
	}

	public int getCustomerIndex(int cus_id) {
		if (index == 0) {
			// Get application context bean CustomerApplication
			ELContext context = FacesContext.getCurrentInstance().getELContext();
			CustomerApplication app = (CustomerApplication) FacesContext.getCurrentInstance().getApplication()
					.getELResolver().getValue(context, null, "customerApplication");
			for (int i = 0; i < app.getCustomers().size(); i++) {
				if (app.getCustomers().get(i).getCusId() == cus_id) {
					index = i;
					break;
				}
			}
			return index;
		}
		return index;
	}

	/**
	 * @param contact Id
	 */
	public void removeContactPerson(cms.entities.CustomerContact contact) {
		try {
			// remove this customer from db via EJB
			int contactId = contact.getContactId();
			int customerId = contact.getCustomer().getCusId();
	        //instantiate customerManagedBean
	        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
	        customerManagedBean = (CustomerManagedBean) FacesContext.getCurrentInstance().getApplication()
	                .getELResolver().getValue(elContext, null, "customerManagedBean");
			customerManagedBean.removeContactPerson(contactId);
			CustomerApplication app = (CustomerApplication) FacesContext.getCurrentInstance().getApplication()
					.getELResolver().getValue(elContext, null, "customerApplication");
			app.searchAll1();
			contactPerson = new ArrayList<cms.entities.CustomerContact>();
			for (int i = 0; i < app.getContactPerson().size(); i++) {
				if (app.getContactPerson().get(i).getCustomer().getCusId() == customerId) {
					contactPerson.add(app.getContactPerson().get(i));
				}
			}
			//index1 = customerId;
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Customer has been deleted succesfully"));
			}
			catch (Exception ex) {
	
	        }
		showForm = true;
	}

}
