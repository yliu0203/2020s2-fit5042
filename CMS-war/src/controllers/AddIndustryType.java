package controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import mbeans.CustomerManagedBean;

@RequestScoped
@Named("addType")
public class AddIndustryType {
	 @ManagedProperty(value = "#{customerManagedBean}")
	 CustomerManagedBean customerManagedBean;
	 
	 CustomerApplication app;
	 private boolean showForm = true;
	 private UIComponent submit;
	 
	 public boolean isShowForm() {
	        return showForm;
	    }
	 public AddIndustryType() {
	        ELContext context
	                = FacesContext.getCurrentInstance().getELContext();

	        app = (CustomerApplication) FacesContext.getCurrentInstance()
	                .getApplication()
	                .getELResolver()
	                .getValue(context, null, "customerApplication");

	        //instantiate customerManagedBean
	        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
	        customerManagedBean = (CustomerManagedBean) FacesContext.getCurrentInstance().getApplication()
	                .getELResolver().getValue(elContext, null, "customerManagedBean");
	    }

	    public void addNewIndustryType(IndustryType localtype) {
	        //this is the local customer, not the entity
	        try {
	            //add this customer to db via EJB
	        	customerManagedBean.addIndustryType(localtype);
	        	
	            //refresh the list in CustomerApplication bean
	             app.searchAll2();
	             FacesMessage message = new FacesMessage("IndustryType has been added succesfully");
	             FacesContext context = FacesContext.getCurrentInstance();
	             context.addMessage(submit.getClientId(context), message);
	            //acesContext.getCurrentInstance().addMessage(null, new FacesMessage("Property has been added succesfully"));
	        } catch (Exception ex) {

	        }
	        showForm = true;
	    }

		/**
		 * @return the submit
		 */
		public UIComponent getSubmit() {
			return submit;
		}

		/**
		 * @param submit the submit to set
		 */
		public void setSubmit(UIComponent submit) {
			this.submit = submit;
		}

}
