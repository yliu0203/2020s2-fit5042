package controllers;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import repository.interfaces.CustomerRepository;
import cms.entities.NormalUser;

@Named(value = "getNormalUser")
@RequestScoped
public class GetNormalUser {
	@EJB
    CustomerRepository UserRepository;

    /**
     * Creates a new instance of GetAllUsers
     */
    public GetNormalUser() {
    }

    public List<NormalUser> getAllStaffs() throws Exception {
        return UserRepository.getAllStaff();

    }

}
