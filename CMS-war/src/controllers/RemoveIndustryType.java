package controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import mbeans.CustomerManagedBean;

@RequestScoped
@Named("removeIndustryType")
public class RemoveIndustryType {
	@ManagedProperty(value = "#{propertyManagedBean}")
    CustomerManagedBean customerManagedBean;

    private boolean showForm = true;

    CustomerApplication app;

    public boolean isShowForm() {
        return showForm;
    }

    public RemoveIndustryType() {
        ELContext context
                = FacesContext.getCurrentInstance().getELContext();

        app = (CustomerApplication) FacesContext.getCurrentInstance()
                .getApplication()
                .getELResolver()
                .getValue(context, null, "customerApplication");

        app.updateIndustryTypesList();

        //instantiate customerManagedBean
        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        customerManagedBean = (CustomerManagedBean) FacesContext.getCurrentInstance().getApplication()
                .getELResolver().getValue(elContext, null, "customerManagedBean");
    }

    /**
     * @param customer Id
     */
    public void deleteIndustryType(int inId) {
        try {
            //remove this customer from db via EJB
        	customerManagedBean.removeIndustryType(inId);
        	
            //refresh the list in CustomerApplication bean
             app.searchAll2();
//             FacesMessage message = new FacesMessage("Customer has been deleted succesfully");
//             FacesContext context = FacesContext.getCurrentInstance();
//             context.addMessage(delete.getClientId(context), message);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Industry Type has been deleted succesfully"));
        } catch (Exception ex) {

        }
        showForm = true;
    }
}
