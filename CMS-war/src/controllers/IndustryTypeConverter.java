package controllers;

import java.util.List;

import javax.el.ELContext;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

import cms.entities.IndustryType;
import mbeans.CustomerManagedBean;

@FacesConverter(forClass = cms.entities.IndustryType.class, value = "industry")
public class IndustryTypeConverter implements Converter{
	@ManagedProperty(value = "#{propertyManagedBean}")
    CustomerManagedBean custoemrManagedBean;

    public List<IndustryType> IndustryTypeList;

    public IndustryTypeConverter() {
        try {
            //instantiate propertyManagedBean
            ELContext elContext = FacesContext.getCurrentInstance().getELContext();
            custoemrManagedBean = (CustomerManagedBean) FacesContext.getCurrentInstance().getApplication()
                    .getELResolver().getValue(elContext, null, "customerManagedBean");

            IndustryTypeList = custoemrManagedBean.getAllIndustryType();
        } catch (Exception ex) {

        }
    }

    //this method is for converting the submitted value (as String) to the Industry Type object
    //the reason for using this method is, the dropdown box in the xhtml does not capture the NormalUser object, but the String.
    public IndustryType getAsObject(FacesContext facesContext, UIComponent component, String submittedValue) {
        if (submittedValue.trim().equals("")) {
            return null;
        } else {
            try {
                int number = Integer.parseInt(submittedValue);

                for (IndustryType c : IndustryTypeList) {
                    if (c.getInId() == number) {
                        return c;
                    }
                }

            } catch (NumberFormatException exception) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid staff"));
            }
        }

        return null;
    }

    public String getAsString(FacesContext facesContext, UIComponent component, Object value) {
        if (value == null || value.equals("")) {
            return "";
        } else {
            return String.valueOf(((IndustryType) value).getInId());
        }
    }


}
