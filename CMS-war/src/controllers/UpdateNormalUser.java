package controllers;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import cms.entities.NormalUser;
import repository.interfaces.CustomerRepository;

@Named(value = "updateNormalUser")
@RequestScoped
public class UpdateNormalUser {
	@EJB
    CustomerRepository UserRepository;
	private NormalUser normalUser;
	
	/**
	    * Creates a new instance of UpdateUser
	    */
	    public UpdateNormalUser() {
	    }
	
    @PostConstruct
    public void init() {
    	normalUser = new NormalUser();
    }
	/**
	 * @return the normalUser
	 */
	public NormalUser getNormalUser() {
		return normalUser;
	}
	/**
	 * @param normalUser the normalUser to set
	 */
	public void setNormalUser(NormalUser normalUser) {
		this.normalUser = normalUser;
	}

    /**
	 * @return the userRepository
	 */
	public CustomerRepository getUserRepository() {
		return UserRepository;
	}

	/**
	 * @param userRepository the userRepository to set
	 */
	public void setUserRepository(CustomerRepository userRepository) {
		UserRepository = userRepository;
	}

	public String ShowDetails(long id) throws Exception {
    	normalUser = UserRepository.selectNormalUserById(id);
        return "normalUserDetial";
    }

    public String UpdateDetails(long id) throws Exception {
    	normalUser = UserRepository.selectNormalUserById(id);
        return "normalUserUpdate";
    }
    
    public String updateStaff() throws Exception {
    	NormalUser normalUser = this.normalUser;
    	NormalUser normalUser1 = UserRepository.selectNormalUserById(normalUser.getId());
    	normalUser.setRoleType("normalUser");
    	normalUser.setAdmin(normalUser1.getAdmin());
    	normalUser.setPassword(sha256(normalUser.getPassword()));
    	UserRepository.updateUser(normalUser);
        return "normalUser";
    }
    
    public String sha256(String password) {
    MessageDigest messageDigest;
    StringBuilder stringBuilder = new StringBuilder();
    try {

    	messageDigest = MessageDigest.getInstance("SHA-256");
        byte[] hashInBytes = messageDigest.digest(password.getBytes(StandardCharsets.UTF_8));
        for (byte bt : hashInBytes) {
        	stringBuilder.append(String.format("%02x", bt));
        }
    } catch (NoSuchAlgorithmException ex) {
        Logger.getLogger(AddNormalUser.class.getName()).log(Level.SEVERE, null, ex);
    }
    return stringBuilder.toString();
    }

}
