package controllers;

import java.io.Serializable;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@RequestScoped
@Named(value = "industryType")
public class IndustryType implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int inId;
	private String industyTypeName;
	
	public IndustryType() {
		
	}
	
	public IndustryType(int inId, String industyTypeName) {
		this.inId = inId;
		this.industyTypeName = industyTypeName;
	}
	/**
	 * @return the inId
	 */
	public int getInId() {
		return inId;
	}
	/**
	 * @param inId the inId to set
	 */
	public void setInId(int inId) {
		this.inId = inId;
	}
	/**
	 * @return the industyTypeName
	 */
	public String getIndustyTypeName() {
		return industyTypeName;
	}
	/**
	 * @param industyTypeName the industyTypeName to set
	 */
	public void setIndustyTypeName(String industyTypeName) {
		this.industyTypeName = industyTypeName;
	}

	@Override
	public String toString() {
		return "IndustryType [inId=" + inId + ", industyTypeName=" + industyTypeName + "]";
	}

//	/**
//	 * @param industryType the industryType to set
//	 */
//	public void setIndustryType(String industryType) {
//		this.industryType = industryType;
//	}
//
//	public String addNewIndustryType() {
//		indusType.setIndustryType(industryType);
//		customerRepository.addType(indusType);
//		return "industry";
//	}
//
//	public String editNewIndustryType(int id) throws Exception {
//		indusType.setIndustryType(industryType);
//		indusType.setInId(id);
//		customerRepository.editType(indusType);
//		return "industry";
//	}
//
//	public String deleteTypeById(int id) throws Exception {
//		customerRepository.deleteType(id);
//		return "industry";
//	}

}
