package controllers;

import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import cms.entities.Admin;
import cms.entities.NormalUser;
import repository.interfaces.CustomerRepository;

@Named(value = "addUser")
@RequestScoped
public class AddNormalUser {
	@EJB
    CustomerRepository UserRepository;
	private NormalUser normalUser;
	private String password;
	private String roleType;
	private String username;
	private Date dob;
	private BigDecimal salary;
	private String samePassword;
    private static boolean isValid = false;
    private String invalidPwdMsg;
	
	//@PostConstruct is an annotation used on a method that 
	//needs to be executed after dependency injection is done to perform any initialization.
    @PostConstruct
    public void init() {
    	normalUser = new NormalUser();
    }
	
	/**
	 * @return the normalUser
	 */
	public NormalUser getNormalUser() {
		return normalUser;
	}

	/**
	 * @param normalUser the normalUser to set
	 */
	public void setNormalUser(NormalUser normalUser) {
		this.normalUser = normalUser;
	}

	/**
	 * @return the userRepository
	 */
	public CustomerRepository getUserRepository() {
		return UserRepository;
	}
	/**
	 * @param userRepository the userRepository to set
	 */
	public void setUserRepository(CustomerRepository userRepository) {
		UserRepository = userRepository;
	}
	public AddNormalUser() {
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @return the roleType
	 */
	public String getRoleType() {
		return roleType;
	}
	/**
	 * @param roleType the roleType to set
	 */
	public void setRoleType(String roleType) {
		this.roleType = roleType;
	}
	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}
	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}
	/**
	 * @return the dob
	 */
	public Date getDob() {
		return dob;
	}
	/**
	 * @param dob the dob to set
	 */
	public void setDob(Date dob) {
		this.dob = dob;
	}
	/**
	 * @return the salary
	 */
	public BigDecimal getSalary() {
		return salary;
	}
	/**
	 * @param salary the salary to set
	 */
	public void setSalary(BigDecimal salary) {
		this.salary = salary;
	}

	/**
	 * @return the samePassword
	 */
	public String getSamePassword() {
		return samePassword;
	}

	/**
	 * @param samePassword the samePassword to set
	 */
	public void setSamePassword(String samePassword) {
		this.samePassword = samePassword;
	}
	
    /**
	 * @return the isValid
	 */
	public static boolean isValid() {
		return isValid;
	}

	/**
	 * @param isValid the isValid to set
	 */
	public static void setValid(boolean isValid) {
		AddNormalUser.isValid = isValid;
	}

	/**
	 * @return the invalidPwdMsg
	 */
	public String getInvalidPwdMsg() {
		return invalidPwdMsg;
	}

	/**
	 * @param invalidPwdMsg the invalidPwdMsg to set
	 */
	public void setInvalidPwdMsg(String invalidPwdMsg) {
		this.invalidPwdMsg = invalidPwdMsg;
	}

	public void listenForComparePwd(String Pwd) {
        if (Pwd.equals(samePassword)) {
        	isValid = true;
        } else {
        	isValid = false;
            invalidPwdMsg = "Two passwords are unequal!!";
        }
    }
	
    public String addNewNormalUser() throws Exception {
    	roleType = "normalUser";
    	List<Admin> admin = UserRepository.getAllAdmin();
    	normalUser.setUsername(username);
    	normalUser.setPassword(sha256(password));
    	normalUser.setDob(dob);
    	normalUser.setSalary(salary);
    	normalUser.setRoleType(roleType);
    	normalUser.setAdmin(admin.get(0));
    	UserRepository.addNormalUser(normalUser);
        return "normalUser";
    }
    
    public String sha256(String password) {
    MessageDigest messageDigest;
    StringBuilder stringBuilder = new StringBuilder();
    try {

    	messageDigest = MessageDigest.getInstance("SHA-256");
        byte[] hashInBytes = messageDigest.digest(password.getBytes(StandardCharsets.UTF_8));
        for (byte bt : hashInBytes) {
        	stringBuilder.append(String.format("%02x", bt));
        }
    } catch (NoSuchAlgorithmException ex) {
        Logger.getLogger(AddNormalUser.class.getName()).log(Level.SEVERE, null, ex);
    }
    return stringBuilder.toString();
    }
	
}
