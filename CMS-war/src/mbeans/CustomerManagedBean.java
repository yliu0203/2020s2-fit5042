package mbeans;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import mbeans.CustomerManagedBean;
import repository.interfaces.CustomerRepository;
import cms.entities.Customer;
import cms.entities.CustomerContact;
import cms.entities.NormalUser;
import cms.entities.Address;
import cms.entities.Admin;
import cms.entities.IndustryType;

@ManagedBean(name = "customerManagedBean")
@SessionScoped
public class CustomerManagedBean implements Serializable{
	@EJB
    CustomerRepository customerRepository;
	
	private UIComponent save;
	private UIComponent save1;
	private UIComponent save2;
    /**
     * Creates a new instance of CustomerManagedBean
     */
    public CustomerManagedBean() {
    }

    public List<Customer> getAllCustomers() {
        try {
            List<Customer> customers = customerRepository.getAllCustomers();
            return customers;
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public List<CustomerContact> getAllContactPerson() {
        try {
            List<CustomerContact> contactPerson = customerRepository.getAllContactPerson();
            return contactPerson;
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public void removeCustomer(int cusId) {
        try {
            customerRepository.removeCustomer(cusId);
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void removeContactPerson(int contactId) {
        try {
            customerRepository.removeContactPerson(contactId);
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void editCustomer(Customer customer) {
        try {
            String s = customer.getAddress().getStreetNumber();
            String s1 = customer.getAddress().getStreetAddress();
            String s2 = customer.getAddress().getSuburb();
            String s3 = customer.getAddress().getState();
            String s4 = customer.getAddress().getPostcode();
            Address address = customer.getAddress();
            address.setStreetNumber(s);
            address.setStreetAddress(s1);
            address.setSuburb(s2);
            address.setState(s3);
            address.setPostcode(s4);
            customer.setAddress(address);
            customerRepository.editCustomer(customer);

            FacesMessage message = new FacesMessage("Customer has been updated succesfully");
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(save.getClientId(context), message);
            //FacesContext.getCurrentInstance().addMessage("MyForm:message", new FacesMessage("Customer has been updated succesfully"));
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void editContactPerson(CustomerContact contactPerson) {
        try {
            customerRepository.editContactPerson(contactPerson);

            FacesMessage message = new FacesMessage("Contact person has been updated succesfully");
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(save1.getClientId(context), message);
            //FacesContext.getCurrentInstance().addMessage("MyForm:message", new FacesMessage("Customer has been updated succesfully"));
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void editIndustryType(IndustryType industryType) {
        try {
            customerRepository.editType(industryType);

            FacesMessage message = new FacesMessage("industry Type has been updated succesfully");
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(save2.getClientId(context), message);
            //FacesContext.getCurrentInstance().addMessage("MyForm:message", new FacesMessage("Customer has been updated succesfully"));
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void removeIndustryType(int inId) {
        try {
            customerRepository.deleteType(inId);
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void addIndustryType(controllers.IndustryType localType) {
        //convert this newCustomer which is the local customer to entity customer
    	IndustryType industryType = convertToEntity2(localType);

        try {
            customerRepository.addType(industryType);
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void addCustomer(controllers.Customer localCustomer) {
        //convert this newCustomer which is the local customer to entity customer
        Customer customer = convertToEntity(localCustomer);

        try {
            customerRepository.addCustomer(customer);
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void addContactPerson(controllers.ContactPerson localContactPerson) {
        //convert this newContactPerson which is the local contact person to entity customerContact
    	CustomerContact customerContact = convertToEntity1(localContactPerson);

        try {
            customerRepository.addContactPerson(customerContact);
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private IndustryType convertToEntity2(controllers.IndustryType localType) {
    	IndustryType industryType = new IndustryType(); //entity
    	industryType.setIndustryType(localType.getIndustyTypeName());
        return industryType;
    }
    
    private CustomerContact convertToEntity1(controllers.ContactPerson localContactPerson) {
    	CustomerContact customerContact = new CustomerContact(); //entity
    	customerContact.setContactId(localContactPerson.getContactId());
    	customerContact.setAge(localContactPerson.getAge());
    	customerContact.setAvailableTime(localContactPerson.getAvailableTime());
    	customerContact.setGender(localContactPerson.getGender());
    	customerContact.setMobilePhone(localContactPerson.getMobilePhone());
    	customerContact.setName(localContactPerson.getName());
    	customerContact.setCustomer(localContactPerson.getCustomer());
        return customerContact;
    }
    
    private Customer convertToEntity(controllers.Customer localCustomer) {
        Customer customer = new Customer(); //entity
        String streetNumber = localCustomer.getStreetNumber();
        String streetAddress = localCustomer.getStreetAddress();
        String suburb = localCustomer.getSuburb();
        String postcode = localCustomer.getPostcode();
        String state = localCustomer.getState();
        Address address = new Address(streetNumber, streetAddress, suburb, postcode, state);
        customer.setAddress(address);
        customer.setCeo(localCustomer.getCeo());
        customer.setIndustryType(localCustomer.getIndustryType());
        customer.setNoOfPeople(localCustomer.getNoOfPeople());
        customer.setPurchasedProduct(localCustomer.getPurchasedProduct());
        customer.setRegisterDate(localCustomer.getRegisterDate());
        customer.setWebsite(localCustomer.getWebsite());
        customer.setNormalUser(localCustomer.getNormalUser());
        //Date dob = localCustomer.getNormalUser().getDob();
        //BigDecimal salary = localCustomer.getNormalUser().getSalary();
        //Admin admin = localCustomer.getNormalUser().getAdmin();
        //NormalUser normalUser = new cms.entities.NormalUser(dob, salary, admin);
//        int conactPersonId = localProperty.getConactPersonId();
//        String name = localProperty.getName();
//        String phoneNumber = localProperty.getPhoneNumber();
//        ContactPerson contactPerson = new fit5042.tutex.repository.entities.ContactPerson(conactPersonId, name, phoneNumber);
//        if (contactPerson.getConactPersonId() == 0) {
//            contactPerson = null;
//        }
//        property.setContactPerson(contactPerson);
        //customer.setNormalUser(normalUser);
        return customer;
    }

	/**
	 * @return the save
	 */
	public UIComponent getSave() {
		return save;
	}

	/**
	 * @param save the save to set
	 */
	public void setSave(UIComponent save) {
		this.save = save;
	}
	
	
    public List<NormalUser> getAllStaff() throws Exception {
        try {
            return customerRepository.getAllStaff();
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    public List<Admin> getAllAdmin() throws Exception {
        try {
            return customerRepository.getAllAdmin();
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }
   
    public List<IndustryType> getAllIndustryType() {
        try {
            return customerRepository.getAllIndustryType();
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }
    
    /**
     * Search a customer by type
     */
    public List<Customer> searchCustomerByType(int type) {
        try {
            return customerRepository.searchCustomerByType(type);
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }
    
    /**
     * Search a customer by purchased product
     */
    public List<Customer> searchCustomerByProduct(String product) {
        try {
            return customerRepository.searchCustomerByProduct(product);
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

	/**
	 * @return the save1
	 */
	public UIComponent getSave1() {
		return save1;
	}

	/**
	 * @param save1 the save1 to set
	 */
	public void setSave1(UIComponent save1) {
		this.save1 = save1;
	}

	/**
	 * @return the save2
	 */
	public UIComponent getSave2() {
		return save2;
	}

	/**
	 * @param save2 the save2 to set
	 */
	public void setSave2(UIComponent save2) {
		this.save2 = save2;
	}
    
}
