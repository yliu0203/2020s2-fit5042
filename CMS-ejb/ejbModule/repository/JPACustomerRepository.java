package repository;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import repository.interfaces.CustomerRepository;
import cms.entities.Admin;
import cms.entities.Customer;
import cms.entities.CustomerContact;
import cms.entities.IndustryType;
import cms.entities.NormalUser;


@Stateless
public class JPACustomerRepository implements CustomerRepository{
	//insert code (annotation) here to use container managed entity manager to complete these methods  
	@PersistenceContext(unitName="CMS-ejb")
    private EntityManager entityManager;
	
	@Override
    public List<Customer> getAllCustomers() throws Exception {
        return entityManager.createNamedQuery(Customer.findAll).getResultList();
    }
	
	@Override
    public List<CustomerContact> getAllContactPerson() throws Exception {
        return entityManager.createNamedQuery(CustomerContact.GET_ALL_CONTACT_NAME).getResultList();
    }

	@Override
    public List<Admin> getAllAdmin() throws Exception {
        return entityManager.createNamedQuery(Admin.GET_ALL_ADMIN).getResultList();
    }
	
    @Override
    public void removeCustomer(int cusId) throws Exception {
    	Customer customer = this.searchCustomerById(cusId);
    	if(!customer.equals(null)) {
    		entityManager.remove(customer);
    	}
    }
    @Override
    public void deleteType(int id) throws Exception {
    	IndustryType industryType = this.searchIndustryById(id);
    	if(!industryType.equals(null)) {
    		entityManager.remove(industryType);
    	}
    }
    
    @Override
    public void removeContactPerson(int contactId) throws Exception {
    	CustomerContact customerContact = this.searchCustomerContactById(contactId);
    	if(!customerContact.equals(null)) {
    		entityManager.remove(customerContact);
    	}
    }
    
    @Override
    public void removeNormalUser(long id) throws Exception {
    	NormalUser normalUser = this.selectNormalUserById(id);
    	if(!normalUser.equals(null)) {
    		entityManager.remove(normalUser);
    	}
    }
    
    @Override
    public CustomerContact searchCustomerContactById(int id) throws Exception {
    	CustomerContact customerContact = entityManager.find(CustomerContact.class, id);
        return customerContact;
    }
    
    
    @Override
    public NormalUser selectNormalUserById(long id) throws Exception {
    	NormalUser normalUser = entityManager.find(NormalUser.class, id);
        return normalUser;
    }
    
    @Override
    public Customer searchCustomerById(int id) throws Exception {
        Customer customer = entityManager.find(Customer.class, id);
        return customer;
    }
    
    @Override
    public IndustryType searchIndustryById(int id) throws Exception {
    	IndustryType industryType = entityManager.find(IndustryType.class, id);
        return industryType;
    }
    
    @Override
    public List<CustomerContact> getAllContactPeople() throws Exception {
        return entityManager.createNamedQuery(CustomerContact.GET_ALL_CONTACT_NAME).getResultList();
    }
    
    @Override
    public void editCustomer(Customer customer) throws Exception {
        try {
            entityManager.merge(customer);
        } catch (Exception ex) {

        }
    }
    
    @Override
    public void updateUser(NormalUser normalUser) throws Exception {
        try {
            entityManager.merge(normalUser);
        } catch (Exception ex) {

        }
    }

    @Override
    public void editContactPerson(CustomerContact customerContact) throws Exception {
        try {
            entityManager.merge(customerContact);
        } catch (Exception ex) {

        }
    }
    
    @Override
    public void editType(IndustryType industryType) throws Exception {
        try {
            entityManager.merge(industryType);
        } catch (Exception ex) {

        }
    }
    
    @Override
    public void addType(IndustryType industryType) {
        entityManager.createNativeQuery("INSERT INTO INDUSTRY_TYPE (INDUSTRYTYPE) "
        		+ "VALUES (?)")
        .setParameter(1, industryType.getIndustryType())
        .executeUpdate();
    }
    
    @Override
    public void addNormalUser(NormalUser normalUser) throws Exception {
        //List<Customer> customers = entityManager.createNamedQuery(Customer.findAll).getResultList();
        //customer.setCusId(customers.get(0).getCusId() + 1);
        //entityManager.persist(customer);
        entityManager.createNativeQuery("INSERT INTO NORMAL_USER (DOB,password,role_type,salary,username,admin_id) "
        		+ "VALUES (?,?,?,?,?,?)")
        .setParameter(1, normalUser.getDob())
        .setParameter(2, normalUser.getPassword())
        .setParameter(3, normalUser.getRoleType())
        .setParameter(4, normalUser.getSalary())
        .setParameter(5, normalUser.getUsername())
        .setParameter(6, normalUser.getAdmin().getId())
        .executeUpdate();
    }
    @Override
    public void addCustomer(Customer customer) throws Exception {
        //List<Customer> customers = entityManager.createNamedQuery(Customer.findAll).getResultList();
        //customer.setCusId(customers.get(0).getCusId() + 1);
        //entityManager.persist(customer);
        entityManager.createNativeQuery("INSERT INTO CUSTOMER (ceo, no_of_people, purchased_product, register_date, website, postcode, state, street_address, street_number, suburb, type_id, staff_id) "
        		+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?)")
        .setParameter(1, customer.getCeo())
        .setParameter(2, customer.getNoOfPeople())
        .setParameter(3, customer.getPurchasedProduct())
        .setParameter(4, customer.getRegisterDate())
        .setParameter(5, customer.getWebsite())
        .setParameter(6, customer.getAddress().getPostcode())
        .setParameter(7, customer.getAddress().getState())
        .setParameter(8, customer.getAddress().getStreetAddress())
        .setParameter(9, customer.getAddress().getStreetNumber())
        .setParameter(10, customer.getAddress().getSuburb())
        .setParameter(11, customer.getIndustryType().getInId())
        .setParameter(12, customer.getNormalUser().getId())
        .executeUpdate();
    }
    
    @Override
    public void addContactPerson(CustomerContact customerContact) throws Exception {
        entityManager.createNativeQuery("INSERT INTO CUSTOMER_CONTACT (AGE, AVAILABLE_TIME, GENDER, MOBILE_PHONE, NAME, CUS_ID) "
        		+ "VALUES (?,?,?,?,?,?)")
        .setParameter(1, customerContact.getAge())
        .setParameter(2, customerContact.getAvailableTime())
        .setParameter(3, customerContact.getGender())
        .setParameter(4, customerContact.getMobilePhone())
        .setParameter(5, customerContact.getName())
        .setParameter(6, customerContact.getCustomer().getCusId())
        .executeUpdate();
    }
    
    @Override
    public List<NormalUser> getAllStaff() throws Exception {
        return entityManager.createNamedQuery(NormalUser.GET_ALL_STAFF).getResultList();
    }
    
    @Override
    public List<IndustryType> getAllIndustryType() throws Exception {
        return entityManager.createNamedQuery(IndustryType.GET_ALL_INDUSTRY_TYPR).getResultList();
    }
    @Override
    public List<Customer> searchCustomerByType(int type) throws Exception {
    	CriteriaBuilder builder = entityManager.getCriteriaBuilder();
    	CriteriaQuery<Customer> cQuery = builder.createQuery(Customer.class);
    	//Metamodel m = entityManager.getMetamodel();
    	//EntityType<Customer> cus = m.entity(Customer.class);
    	Root<Customer> e = cQuery.from(Customer.class);
    	cQuery.where(builder.equal(e.get("industryType").get("inId"), type));
    	TypedQuery<Customer> typedQuery = entityManager.createQuery(cQuery);
    	//TypedQuery tQuery = entityManager.createQuery(cQuery);
        //complete this method using Criteria API
        return typedQuery.getResultList();
    }
    
    /**
     * Search for customers by its purchased product and Criteria API
     *
     * @param product - the product of the customer to search for
     * @return the customer found
     */
    @Override
    public List<Customer> searchCustomerByProduct(String product) throws Exception {
    	return entityManager.createNamedQuery(Customer.findOneProduct).setParameter("product", product).getResultList();
//    	TypedQuery<Customer> query = entityManager.createQuery("SELECT p FROM CUSTOMER p WHERE p.PURCHASED_PRODUCT = :product",Customer.class);
//    	List<Customer> customers = query.setParameter("product", product).getResultList();
//    	return customers;
//    	CriteriaBuilder builder = entityManager.getCriteriaBuilder();
//    	CriteriaQuery<Customer> cQuery = builder.createQuery(Customer.class);
//    	Root<Customer> e = cQuery.from(Customer.class);
//    	cQuery.where(builder.equal(e.get("purchasedProduct"), product));
//    	TypedQuery<Customer> typedQuery = entityManager.createQuery(cQuery);
//        return typedQuery.getResultList();
    }

}
