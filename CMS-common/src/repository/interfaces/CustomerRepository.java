package repository.interfaces;

import java.util.List;

import javax.ejb.Remote;

import cms.entities.Admin;
import cms.entities.Customer;
import cms.entities.CustomerContact;
import cms.entities.NormalUser;
import cms.entities.IndustryType;

/**
*
* @author Rock
*/
@Remote
public interface CustomerRepository {


    /**
     * Return all the customers in the repository
     *
     * @return all the customers in the repository
     */
    public List<Customer> getAllCustomers() throws Exception;

    /**
     * Return all the Admin in the repository
     *
     * @return all the Admin in the repository
     */
    public List<Admin> getAllAdmin() throws Exception;
    
    /**
     * Return all the CustomerContact in the repository
     *
     * @return all the CustomerContact in the repository
     */
    public List<CustomerContact> getAllContactPerson() throws Exception;
    
    
    /**
     * Remove the customer, whose customer ID matches the one being passed as
     * parameter, from the repository
     *
     * @param cusId - the ID of the customer to remove
     */
    public void removeCustomer(int cusId) throws Exception;

    /**
     * Remove the NormalUser, whose NormalUser ID matches the one being passed as
     * parameter, from the repository
     *
     * @param id - the ID of the NormalUser to remove
     */
    public void removeNormalUser(long id) throws Exception;
    
    /**
     * Remove the industry type, whose type ID matches the one being passed as
     * parameter, from the repository
     *
     * @param id - the ID of the industry type to remove
     */
    public void deleteType(int id) throws Exception;
    
    /**
     * Remove the Contact Person, whose Contact Person ID matches the one being passed as
     * parameter, from the repository
     *
     * @param contactId - the ID of the Contact Person to remove
     */
    public void removeContactPerson(int contactId) throws Exception;
    
    
    /**
     * Search for a customer by its customer ID
     *
     * @param id - the customerId of the customer to search for
     * @return the customer found
     */
    public Customer searchCustomerById(int id) throws Exception;
    
    /**
     * Search for a IndustryType by its IndustryType ID
     *
     * @param id - the IndustryType id of the IndustryType to search for
     * @return the IndustryType found
     */
    public IndustryType searchIndustryById(int id) throws Exception;
    

    /**
     * Search for a NormalUser by its NormalUser ID
     *
     * @param id - the NormalUser id of the NormalUser to search for
     * @return the NormalUser found
     */
    public NormalUser selectNormalUserById(long id) throws Exception;
    
    
    /**
     * Search for a Contact Person by its ID
     *
     * @param id - the contactId of the Contact Person to search for
     * @return the contactPerson found
     */
    public CustomerContact searchCustomerContactById(int id) throws Exception;
    
    /**
     * Return all the contact people in the repository
     *
     * @return all the contact people in the repository
     */
    public List<CustomerContact> getAllContactPeople() throws Exception;
    
    /**
     * Update a customer in the repository
     *
     * @param customer - the updated information regarding a customer
     */
    public void editCustomer(Customer customer) throws Exception;
    
    
    /**
     * Update a Normal User in the repository
     *
     * @param NormalUser - the updated information regarding a NormalUser
     */
    public void updateUser(NormalUser normalUser) throws Exception;
    
    
    /**
     * Update a contact person in the repository
     *
     * @param contactPerson - the updated information regarding a contact person
     */
    public void editContactPerson(CustomerContact customerContact) throws Exception;
    
    /**
     * Add the customer being passed as parameter into the repository
     *
     * @param customer - the customer to add
     */
    public void addCustomer(Customer customer) throws Exception;
    
    /**
     * Add the normalUser being passed as parameter into the repository
     *
     * @param normalUser - the normalUser to add
     */
    public void addNormalUser(NormalUser normalUser) throws Exception;
    
    /**
     * Add the IndustryType being passed as parameter into the repository
     *
     * @param industryType - the industryType to add
     */
    public void addType(IndustryType industryType);
    
    /**
     * Add the IndustryType being passed as parameter into the repository
     *
     * @param industryType - the industryType to add
     */
    public void editType(IndustryType industryType) throws Exception;;
    
    
    /**
     * Add the customer being passed as parameter into the repository
     *
     * @param customer - the customer to add
     */
    public void addContactPerson(CustomerContact customerContact) throws Exception;
    
    
    /**
     * Return all the staff in the repository
     *
     * @return all the staff in the repository
     */
    public List<NormalUser> getAllStaff() throws Exception;
    /**
     * Return all the Industry type in the repository
     *
     * @return all the Industry type in the repository
     */
    public List<IndustryType> getAllIndustryType() throws Exception;
    
    /**
     * Search for customers by its type
     *
     * @param type - the type of the customer to search for
     * @return the customer found
     */
    public List<Customer> searchCustomerByType(int type) throws Exception;
    
    
    /**
     * Search for customers by its purchased product
     *
     * @param product - the product of the customer to search for
     * @return the customer found
     */
    public List<Customer> searchCustomerByProduct(String product) throws Exception;
}
