package cms.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the CUSTOMER_CONTACT database table.
 * 
 */
@Entity
@Table(name="CUSTOMER_CONTACT")
@NamedQuery(name=CustomerContact.GET_ALL_CONTACT_NAME, query="SELECT c FROM CustomerContact c order by c.contactId ASC")
public class CustomerContact implements Serializable {
	
    public static final String GET_ALL_CONTACT_NAME = "ContactPerson.getAll";
	private static final long serialVersionUID = 1L;
	private int contactId;
	private int age;
	private String availableTime;
	private String gender;
	private int mobilePhone;
	private String name;
	private Customer customer;

	public CustomerContact() {
	}

	public CustomerContact(int contactId, int age, String availableTime, String gender, int mobilePhone, String name,
			Customer customer) {
		this.contactId = contactId;
		this.age = age;
		this.availableTime = availableTime;
		this.gender = gender;
		this.mobilePhone = mobilePhone;
		this.name = name;
		this.customer = customer;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	@Column(name="CONTACT_ID")
	public int getContactId() {
		return this.contactId;
	}

	public void setContactId(int contactId) {
		this.contactId = contactId;
	}


	public int getAge() {
		return this.age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	//In plain Java APIs, the temporal precision of time is not defined. 
	//When dealing with temporal data, you might want to describe the expected precision in database. 
	//Temporal data can have DATE, TIME, or TIMESTAMP precision (i.e., the actual date, only the time, or both). 
	//Use the @Temporal annotation to fine tune that.
	//@Temporal(TemporalType.TIMESTAMP)
	@Column(name="AVAILABLE_TIME")
	public String getAvailableTime() {
		return this.availableTime;
	}

	public void setAvailableTime(String availableTime) {
		this.availableTime = availableTime;
	}


	public String getGender() {
		return this.gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}


	@Column(name="MOBILE_PHONE")
	public int getMobilePhone() {
		return this.mobilePhone;
	}

	public void setMobilePhone(int mobilePhone) {
		this.mobilePhone = mobilePhone;
	}


	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}


	//bi-directional many-to-one association to Customer
	@ManyToOne(cascade={CascadeType.PERSIST})
	@JoinColumn(name="CUS_ID")
	public Customer getCustomer() {
		return this.customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((customer == null) ? 0 : customer.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CustomerContact other = (CustomerContact) obj;
		if (customer == null) {
			if (other.customer != null)
				return false;
		} else if (!customer.equals(other.customer))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CustomerContact [age=" + age + ", availableTime=" + availableTime + ", gender=" + gender
				+ ", mobilePhone=" + mobilePhone + ", name=" + name + "]";
	}

}