package cms.entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
/**
*
* @author Rock
*/
@Entity
@Table(name="INDUSTRY_TYPE")
@NamedQuery(name=IndustryType.GET_ALL_INDUSTRY_TYPR, query="SELECT DISTINCT c FROM IndustryType c order by c.inId ASC")
public class IndustryType implements Serializable {
	/**
	 * 
	 */
    public static final String GET_ALL_INDUSTRY_TYPR = "IndustryType.getAll";
	private static final long serialVersionUID = 1L;
	private int inId;
	private String industryType;
	private Set<Customer> customer;

	public IndustryType() {
	}

	public IndustryType(int inId, String industryType, Set<Customer> customer) {
		this.inId = inId;
		this.industryType = industryType;
		this.customer = customer;
	}

	/**
	 * @return the industryType
	 */
	public String getIndustryType() {
		return industryType;
	}

	/**
	 * @param industryType the industryType to set
	 */
	public void setIndustryType(String industryType) {
		this.industryType = industryType;
	}

	/**
	 * @return the inId
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	@Column(name="TYPE_ID")
	public int getInId() {
		return inId;
	}

	/**
	 * @param inId the inId to set
	 */
	public void setInId(int inId) {
		this.inId = inId;
	}

	/**
	 * @return the customer
	 */
	//bi-directional many-to-one association to CustomerContact
	//orphanRemoval=true is an entirely ORM-specific thing. It marks "child" entity to be removed when it's no longer referenced from the "parent" entity,
	@OneToMany(mappedBy="industryType", cascade={CascadeType.PERSIST}, fetch=FetchType.EAGER,orphanRemoval=true)
	public Set<Customer> getCustomer() {
		return customer;
	}

	/**
	 * @param customer the customer to set
	 */
	public void setCustomer(Set<Customer> customer) {
		this.customer = customer;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + inId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IndustryType other = (IndustryType) obj;
		if (inId != other.inId)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "IndustryType [inId=" + inId + ", industryType=" + industryType + ", customer=" + customer + "]";
	}
}
