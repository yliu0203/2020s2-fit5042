package cms.entities;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.persistence.*;

import cms.entities.Address;

import java.util.Calendar;
import java.util.Date;
import java.util.Set;


/**
 * The persistent class for the CUSTOMER database table.
 * 
 */
@Entity
@Table(name="customer")
//@NamedQuery(name="Customer.findAll", query="SELECT c FROM Customer c")
@NamedQueries({
    @NamedQuery(name = Customer.findAll, query = "SELECT c FROM Customer c order by c.cusId ASC"),
    @NamedQuery(name = Customer.findOneProduct, query = "SELECT p FROM Customer p WHERE p.purchasedProduct = :product")})
public class Customer implements Serializable {
	
	public static final String findAll = "Customer.getAll";
	public static final String findOneProduct = "Customer.getOneProduct";
	private static final long serialVersionUID = 1L;
	private int cusId;
	private String ceo;
	private IndustryType industryType;
//	private String industryType;
	private int noOfPeople;
	private String purchasedProduct;
	private Date registerDate;
	private String website;
	private Set<CustomerContact> customerContacts;
	private Address address;
	private NormalUser normalUser;
    public Customer(int cusId, String ceo, IndustryType industryType, int noOfPeople, String purchasedProduct,
			Date registerDate, String website, Set<CustomerContact> customerContacts, Address address,
			NormalUser normalUser) {
		this.cusId = cusId;
		this.ceo = ceo;
		this.industryType = industryType;
		this.noOfPeople = noOfPeople;
		this.purchasedProduct = purchasedProduct;
		this.registerDate = registerDate;
		this.website = website;
		this.customerContacts = customerContacts;
		this.address = address;
		this.normalUser = normalUser;
	}

	/**
	 * @return the normalUser
	 */
	//bi-directional many-to-one association to Customer
    @ManyToOne(cascade={CascadeType.PERSIST})
	@JoinColumn(name="STAFF_ID",nullable=false)
	public NormalUser getNormalUser() {
		return normalUser;
	}

	/**
	 * @param normalUser the normalUser to set
	 */
	public void setNormalUser(NormalUser normalUser) {
		this.normalUser = normalUser;
	}

	public Customer() {
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	@Column(name="CUS_ID")
	public int getCusId() {
		return this.cusId;
	}

	public void setCusId(int cusId) {
		this.cusId = cusId;
	}


	public String getCeo() {
		return this.ceo;
	}

	public void setCeo(String ceo) {
		this.ceo = ceo;
	}


//	@Column(name="INDUSTRY_TYPE")
//	public String getIndustryType() {
//		return this.industryType;
//	}
//
//	public void setIndustryType(String industryType) {
//		this.industryType = industryType;
//	}


	/**
	 * @return the industryType
	 */
	//bi-directional many-to-one association to Customer
	@ManyToOne(cascade={CascadeType.PERSIST})
	@JoinColumn(name="TYPE_ID")
	public IndustryType getIndustryType() {
		return industryType;
	}

	/**
	 * @param industryType the industryType to set
	 */
	public void setIndustryType(IndustryType industryType) {
		this.industryType = industryType;
	}

	@Column(name="NO_OF_PEOPLE")
	public int getNoOfPeople() {
		return this.noOfPeople;
	}

	public void setNoOfPeople(int noOfPeople) {
		this.noOfPeople = noOfPeople;
	}


	@Column(name="PURCHASED_PRODUCT")
	public String getPurchasedProduct() {
		return this.purchasedProduct;
	}

	public void setPurchasedProduct(String purchasedProduct) {
		this.purchasedProduct = purchasedProduct;
	}


	@Temporal(TemporalType.DATE)
	@Column(name="REGISTER_DATE")
	public Date getRegisterDate() {
		return this.registerDate;
	}

	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	public String getWebsite() {
		return this.website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}


	//bi-directional many-to-one association to CustomerContact
	//orphanRemoval=true is an entirely ORM-specific thing. It marks "child" entity to be removed when it's no longer referenced from the "parent" entity,
	@OneToMany(mappedBy="customer", cascade={CascadeType.PERSIST}, fetch=FetchType.EAGER,orphanRemoval=true)
	public Set<CustomerContact> getCustomerContacts() {
		return this.customerContacts;
	}

	public void setCustomerContacts(Set<CustomerContact> customerContacts) {
		this.customerContacts = customerContacts;
	}

	public CustomerContact addCustomerContact(CustomerContact customerContact) {
		getCustomerContacts().add(customerContact);
		customerContact.setCustomer(this);

		return customerContact;
	}

	public CustomerContact removeCustomerContact(CustomerContact customerContact) {
		getCustomerContacts().remove(customerContact);
		customerContact.setCustomer(null);

		return customerContact;
	}


    //insert annotation here to make addess as embeded to Property entity and stored as part of Property
    @Embedded
    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + cusId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Customer other = (Customer) obj;
		if (cusId != other.cusId)
			return false;
		return true;
	}

	@Override
	public String toString() {
		String pattern = "MM/dd/yyyy";

		// Create an instance of SimpleDateFormat used for formatting 
		// the string representation of date according to the chosen pattern
		DateFormat df = new SimpleDateFormat(pattern);       
		// Using DateFormat format method we can create a string 
		// representation of a date with the defined format.
		String registered = df.format(registerDate);
		return "Company CEO: " + ceo + "<br />"+ "Industry Type: " + industryType.getIndustryType() + "<br />"+ "Purchased Product: " + purchasedProduct
				+ "<br />"+ "Register Date: " + registered + "<br />"+ "Address: " + address;
	}

}