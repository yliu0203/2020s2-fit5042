
package cms.entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import cms.entities.NormalUser;

/**
 *
 * @author Rock
 */
@Entity
@Table(name="Administrator")
@NamedQueries({
    @NamedQuery(name = Admin.GET_ALL_ADMIN, query = "SELECT n FROM Admin n")})
public class Admin extends User implements Serializable {

	private static final long serialVersionUID = 1L;
	public static final String GET_ALL_ADMIN = "Admin.getAll";
	//private int adminId;
	private String adminAddress;
	private int adminPhone;
	private Set<NormalUser> normalUsers;
    
	public Admin() {
	}
	
	public Admin(String password, String roleType, String username, int adminId, String adminAddress, int adminPhone,
			Set<NormalUser> normalUsers) {
		super(password, roleType, username);
		//this.adminId = adminId;
		this.adminAddress = adminAddress;
		this.adminPhone = adminPhone;
		this.normalUsers = normalUsers;
	}

//	/**
//	 * @return the adminId
//	 */
//	@Column(name="ADMIN_ID")
//	public int getAdminId() {
//		return adminId;
//	}
//
//	/**
//	 * @param adminId the adminId to set
//	 */
//	public void setAdminId(int adminId) {
//		this.adminId = adminId;
//	}

	/**
	 * @return the normalUsers
	 */
	//bi-directional many-to-one association to Normal_User
	@OneToMany(mappedBy="admin", cascade={CascadeType.PERSIST}, fetch=FetchType.EAGER)
	public Set<NormalUser> getNormalUsers() {
		return normalUsers;
	}

	/**
	 * @param normalUsers the normalUsers to set
	 */
	public void setNormalUsers(Set<NormalUser> normalUsers) {
		this.normalUsers = normalUsers;
	}

	/**
	 * @return the adminAddress
	 */
	@Column(name="admin_address")
	public String getAdminAddress() {
		return adminAddress;
	}

	/**
	 * @param adminAddress the adminAddress to set
	 */
	public void setAdminAddress(String adminAddress) {
		this.adminAddress = adminAddress;
	}

	/**
	 * @return the adminPhone
	 */
	@Column(name="admin_phone")
	public int getAdminPhone() {
		return adminPhone;
	}

	/**
	 * @param adminPhone the adminPhone to set
	 */
	public void setAdminPhone(int adminPhone) {
		this.adminPhone = adminPhone;
	}

}
