/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cms.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import cms.entities.Admin;

/**
 *
 * @author rock
 */
@Entity
@Table(name="Normal_User")
@NamedQueries({
    @NamedQuery(name = NormalUser.GET_ALL_STAFF, query = "SELECT n FROM NormalUser n")})
public class NormalUser extends User implements Serializable {

	private static final long serialVersionUID = 1L;
	//private int staffId;
	public static final String GET_ALL_STAFF = "NormalUser.getAll";
	private Date dob;
	private BigDecimal salary;
	private Admin admin;
	private Set<Customer> customers;

	public NormalUser(String password, String roleType, String username,Date dob,
			BigDecimal salary, Admin admin) {
		super(password, roleType, username);
		this.dob = dob;
		this.salary = salary;
		this.admin = admin;
	}


	/**
	 * @return the customers
	 */
	//bi-directional many-to-one association to CustomerContact
	//@OneToMany(mappedBy="normalUser", cascade={CascadeType.ALL}, fetch=FetchType.EAGER,orphanRemoval = true)
	@OneToMany(mappedBy="normalUser", cascade={CascadeType.PERSIST}, fetch=FetchType.EAGER,orphanRemoval = true)
	public Set<Customer> getCustomers() {
		return customers;
	}


	/**
	 * @param customers the customers to set
	 */
	public void setCustomers(Set<Customer> customers) {
		this.customers = customers;
	}


	public NormalUser() {
	}
	
//	@Column(name="STAFF_ID")
//	public int getStaffId() {
//		return this.staffId;
//	}
//
//	public void setStaffId(int staffId) {
//		this.staffId = staffId;
//	}


	@Temporal(TemporalType.DATE)
	public Date getDob() {
		return this.dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}


	public BigDecimal getSalary() {
		return this.salary;
	}

	public void setSalary(BigDecimal salary) {
		this.salary = salary;
	}

	//bi-directional many-to-one association to Admin
	@ManyToOne(cascade={CascadeType.PERSIST})
	@JoinColumn(name="ADMIN_ID")
	public Admin getAdmin() {
		return this.admin;
	}

	public void setAdmin(Admin admin) {
		this.admin = admin;
	}
    @Override
    public int hashCode() {
        long hash = 7;
        hash = 53 * hash + this.getId();
        return (int) hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final NormalUser other = (NormalUser) obj;
        if (this.getId() != other.getId()) {
            return false;
        }
        return true;
    }

	@Override
	public String toString() {
		return "Staff Name: "+getUsername()+"<br />"+"Staff Id: "+ getId() + "<br />"+ "Login role Type: "+ getRoleType();
	}

}
